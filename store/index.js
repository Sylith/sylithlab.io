export const state = () => ({
  tags: [],
  articles: null,

  arr: [],
  query: '',

  totalItems: 1,
  currentPage: 1,
  size: 5,
})

export const mutations = {
  updateArticles(state, articles) {
    state.articles = articles
  },
  updateTags(state, tags) {
    state.tags = tags.sort()
  },

  updateArr(state, tag) {
    if (state.arr.includes(tag)) {
      state.arr = state.arr.filter((elem) => elem !== tag)
    } else {
      state.arr.push(tag)
    }
  },
  updateQuery(state, query) {
    state.query = query
  },
  updateTotalItems(state, items) {
    state.totalItems = items
  },
  updateCurrentPage(state, page) {
    state.currentPage = page
  },
}

export const actions = {
  async allTags(context) {
    const raw = await this.$content('articles')
      .only(['tags'])
      .fetch()
      .catch((err) => {
        error({ statusCode: 404, message: 'Failed to fetch tags' })
      })

    let unFilteredTags = []
    raw.forEach((element) => {
      element.tags.forEach((el) => {
        unFilteredTags.push(el)
      })
    })
    const tags = unFilteredTags.filter((c, index) => {
      return unFilteredTags.indexOf(c) === index
    })
    context.commit('updateTags', tags)
  },

  // async allArticles(context) {
  //   const items = await this.$content('articles', { deep: true })
  //     .only([])
  //     .fetch()
  //   context.commit('updateTotalItems', items.length)

  //   const articles = await this.$content('articles', { deep: true })
  //     .only(['title', 'slug', 'tags'])
  //     .skip((context.state.currentPage - 1) * context.state.size)
  //     .limit(context.state.size)
  //     .fetch()
  //     .catch((err) => {
  //       error({ statusCode: 404, message: 'Page not found' })
  //     })

  //   context.commit('updateArticles', articles)
  // },

  async searchArticles(context) {
    if (context.state.query.length > 0) {
      const articles = await this.$content('articles')
        .only(['title', 'slug', 'tags', 'description', 'createdAt'])
        .where({ tags: { $contains: context.state.arr } })
        .search(context.state.query)
        .fetch()
        .catch((err) => {
          error({
            statusCode: 404,
            message: 'Error occured while attempting to find posts',
          })
        })
      context.commit('updateTotalItems', articles.length)
      context.commit('updateArticles', articles)
    } else {
      const articles = await this.$content('articles')
        .only(['title', 'slug', 'tags', 'description', 'createdAt'])
        .where({ tags: { $contains: context.state.arr } })
        .fetch()
        .catch((err) => {
          error({
            statusCode: 404,
            message: 'Error occured while attempting to find posts',
          })
        })
      context.commit('updateTotalItems', articles.length)
      context.commit('updateArticles', articles)
    }
  },
  // async tagedArticles(context, payload) {
  //   const articles = await this.$content('articles', { deep: true })
  //     .only(['title', 'slug', 'tags'])
  //     .where({ tags: { $contains: payload.arr } })
  //     .fetch()
  //     .catch((err) => {
  //       error({ statusCode: 404, message: 'Page not found' })
  //     })

  //   context.commit('updateArticles', articles)
  // },
  // async searchArticles(context, payload) {
  //   const articles = await this.$content('articles', { deep: true })
  //     .only(['title', 'slug', 'tags'])
  //     .search(payload.query)
  //     .fetch()
  //     .catch((err) => {
  //       error({
  //         statusCode: 404,
  //         message: 'Error occured while searching for articles',
  //       })
  //     })

  //   context.commit('updateArticles', articles)
  // },
}

export const getters = {
  getTags(state) {
    return state.tags
  },
  getArticles(state) {
    return state.articles
  },
}
