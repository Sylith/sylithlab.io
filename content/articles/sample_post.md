---
title: 'Sample post'
description: 'This is a sample post just to test things out'
tags:
  - first
  - dogs
  - memes
  - markdown
  - post
  - md
---

Simple text

# This heading 1

## THis is heading 2

### This is heading 3

_italic text_

**bold text**

- list item 1
- list item 2
  - sub item 1
  - sub item 2
- list item 3

1. another list item 1
2. another list item 2
   1. sub item 1
   2. sub item 2
3. another list item 3

Link to [yandex](https://ya.ru)

external image:

![external image](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fyt3.ggpht.com%2F-FJXWJ1x1bEQ%2FAAAAAAAAAAI%2FAAAAAAAAAAA%2FZtyuZ-elFr4%2Fs900-c-k-no-mo-rj-c0xffffff%2Fphoto.jpg&f=1&nofb=1)

internal image:

![internal image](../images/sample_post/photo_2020-08-23_14-33-06.jpg)

`<h1>` an inline code

---

code:

```css
.cssselector {
  margin: 0;
}
```

Tasks:

- [ ] task one
- [x] completed task
