---
title: 'Do not hesitate'
description: 'Motivational reminder to myself'
tags:
  - thoughts
  - personal
  - post
---

I was struggling to come up with some interesting tech related topic for the first blog post. There must something I am good at, something I've been working on for enough time to master it. If I want to teach something others, I must be a hundred percent confident that what I'm saying is absolutely correct thus I must understand the subject thoroughly to deliver the best. In addition, the topic I'm going to cover has to be unique.

Turns out there's no such topic... I mean I have some knowledge (just like everybody) but it's like swimming in a paddling pool whereas I should dive much deeper and start getting pearls out of shellfish. Undoubtedly, I could write about basic stuff, however I can barely see any sense doing that. Everything has already been covered millions times by experts.

That made me think: am I indeed good at anything? Do I really know anything? Am I too weak, are my efforts worth anything? I suppose you have also faced this kind of questions at some period of your life as I do. Moreover, I reckon that this doubts in me won't just go away even when I'll reach a certain level of professionalism. You know, they say, the more skillful you become, the more knowledge you get — the more you realize that you actually know almost nothing and the more questions you get. Some kind of Socratic paradox.

You can never be sure that you've reached perfection. What you can do about it? Just accept it. After all, the amount of your knowledge fades in comparison to your zeal. In my personal opinion, constant self-improvement is the key.

Alright, but what about uniqueness? The term 'uniqueness' itself is vague. From my perspective, almost everything person creates is simply a compilation of what one saw (felt, listened, experienced e.t.c.) earlier. The percentage of something original is incredibly low. New artist learn through copying, eventually this will help them to produce new, their own, style. The whole point is to gather as much information as possible, try to recreate as much as you can, and don't worry about originality (as long as you provide credits to author), because someday you might become the one to be copied.

'So what's the point of all these words?' — you may ask. First of all this is a reminder to myself: never let any hesitations break you down, keep moving forward destroying every obstacle on your path without doubt. You may not see this now, but someday your hard work will definitely pay off. Fuegoleon Vermillion taught: **"Being dissatisfied with your current level doesn't make you a liability, doing nothing about it does!"**

![fueolon](https://i.ytimg.com/vi/3BQskmR4zyw/maxresdefault.jpg)
